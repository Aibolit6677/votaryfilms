=== Brook ===

Contributors: thememove
Tags: editor-style, featured-images, microformats, post-formats, rtl-language-support, sticky-post, threaded-comments, translation-ready

Requires at least: 4.4
Tested up to: 4.9.x
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Brook – Creative Multipurpose WordPress Theme

== Description ==

Brook embraces a modern look with various enhanced shortcodes, premium plugins and pre-defined page elements.

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =


== Changelog ==

= 1.3.0 - November 10 2018 =
1. Add preset templates saved to WPBakery Page Builder.
2. Improvement dashboard screen.
3. Fix footer compatible with bbPress plugin.
4. Fix single product slider js bug.
5. Fix some settings in Customize missing label.

= 1.2.3 - November 07 2018 =
1. Fix minor bug with Customize.
2. Improvement back-end performance.
3. Add WPML language switcher to headers.

= 1.2.2 - November 03 2018 =
1. Fix blog masonry style.
2. Fix tilt effect on News Bulletin homepage.
3. Update Insight Core plugin - v.1.5.4.2. Improvement back-end performance.
4. Disable WP Embeds by default. Improvement front-end performance.

= 1.2.1 - November 01 2018 =
1. Add option show/hide video popup on single portfolio page.
2. Add group action/hook to easy custom theme.
3. Update out-date template files. Compatible with Woocommerce latest version v3.5.1
4. Add option controls sticky product feature image/details columns.

= 1.2.0 - October 31 2018 =
1. Add new 5 homepages ( Expert + Revolutionary + Astronomy + Authentic Studio + Foodie )
2. Add option show/hide caption for single portfolio gallery.
3. Remove default page content spacing of Page Template.
4. Add more options for Coming Soon + Maintenance templates.
5. Add Vc row scrolling background effect.
6. Fix minor style.

= 1.1.3 - October 25 2018 =
1. Fix minor shop style.
2. Update out-date template files. Compatible with Woocommerce latest version v3.5.0
3. Improvement UI for Customize -> Advanced -> Pre Loader
4. Fix vc row full height (case not calculated) on handheld Safari browser.
5. Optimize translate string.

= 1.1.2 - October 24 2018 =
1. Fix shop catalog display option not work properly.
2. Fix vc row full height on handheld Safari browser.
3. Better UI to easy activate maintenance mode.
4. Optimize code.

= 1.1.1 - October 23 2018 =
1. Better front-end performance.
2. Update Insight Core plugin - v.1.5.4.1

= 1.1.0 - October 22 2018 =
1. Add 2 new homepages (News Bulletin + Digital Broadsheets).
2. Fix wrong label of social networks setting in Customize Maintenance.
3. Better instruction for social networks settings in Customize.
4. Improvement front-end performance.
5. Fix minor issue style of Woocommerce Compare.
6. Fix typography not working properly.
7. Add display product categories on shop catalog page.

= 1.0.4 - October 18 2018 =
1. Add hook/action to easy add custom font to Customize.
2. Disable WP Emoji by default for better performance.

= 1.0.3 - October 16 2018 =
1. Fix WPBakery Front-end Editor.
2. Improvement page loading speed.
3. Fix admin auto update theme not working.

= 1.0.2 - October 13 2018 =
1. Fix php warning when woocommerce plugin not activated.

= 1.0.1 - October 12 2018 =
1. Update demo content import.
2. Update WPBakery Page Builder plugin - v.5.5.5

= 1.0.0 - October 12 2018 =
* Initial release

== Credits ==

* Based on Underscores http://underscores.me/, (C) 2012-2015 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* normalize.css http://necolas.github.io/normalize.css/, (C) 2012-2015 Nicolas Gallagher and Jonathan Neal, [MIT](http://opensource.org/licenses/MIT)
