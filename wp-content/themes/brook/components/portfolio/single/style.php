<?php
$portfolio_url     = Brook_Helper::get_post_meta( 'portfolio_url', '' );
$portfolio_gallery = Brook_Helper::get_post_meta( 'portfolio_gallery', '' );

$class = 'portfolio-content-wrap col-xs-order-second';

if ( $portfolio_gallery !== '' || has_post_thumbnail() ) {
	$class .= ' col-md-4';
} else {
	$class .= ' col-md-12';
}
?>

<div class="row tm-sticky-parent">
	<div class="<?php echo esc_attr( $class ); ?>">
		<div class="tm-sticky-column">
			<div class="portfolio-details-content">
				<div class="portfolio-main-info">
					<h3 class="portfolio-title"><?php the_title(); ?></h3>

					<div class="portfolio-content">
						<h6 class="portfolio-about-project-label"><?php esc_html_e( 'About the project', 'brook' ); ?></h6>

						<?php the_content(); ?>
					</div>

					<?php Brook_Templates::portfolio_view_project_button( $portfolio_url ); ?>
				</div>

				<?php Brook_Templates::portfolio_details(); ?>

				<?php Brook_Templates::portfolio_sharing(); ?>
			</div>
		</div>
	</div>

	<?php if ( $portfolio_gallery !== '' || has_post_thumbnail() ) : ?>
		<div class="portfolio-feature-wrap col-lg-7 col-lg-push-1 col-md-8 col-xs-order-first">
			<div class="tm-sticky-column">

				<?php Brook_Portfolio::portfolio_video( array( 'position' => 'above' ) ); ?>

				<div class="portfolio-details-gallery">

					<?php
					$caption_enable = Brook::setting( 'single_portfolio_feature_caption' );
					$caption_enable = $caption_enable === '1' ? true : false;
					?>

					<?php if ( has_post_thumbnail() ) : ?>
						<div class="portfolio-image">
							<?php
							Brook_Image::the_post_thumbnail( array(
								'size'           => 'custom',
								'width'          => 670,
								'height'         => 9999,
								'crop'           => false,
								'caption_enable' => $caption_enable,
							) );
							?>
						</div>
					<?php endif; ?>

					<?php if ( $portfolio_gallery !== '' ) : ?>
						<?php foreach ( $portfolio_gallery as $key => $value ) { ?>
							<div class="portfolio-image">
								<?php Brook_Image::the_attachment_by_id( array(
									'id'             => $value['id'],
									'size'           => 'custom',
									'width'          => 670,
									'height'         => 9999,
									'crop'           => false,
									'caption_enable' => $caption_enable,
								) );
								?>
							</div>
						<?php } ?>
					<?php endif; ?>

				</div>

				<?php Brook_Portfolio::portfolio_video( array( 'position' => 'below' ) ); ?>

			</div>
		</div>
	<?php endif; ?>
</div>
