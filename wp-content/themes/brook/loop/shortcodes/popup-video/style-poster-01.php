<?php
$poster = $image_size = $image_size_width = $image_size_height = $video_text = '';
extract( $brook_shortcode_atts );
?>

<a href="<?php echo esc_url( $video ); ?>" class="video-link">
	<div class="video-poster">

		<div class="video-overlay">
			<div class="video-button">
				<div class="video-play">
					<span class="video-play-icon"></span>
				</div>

				<?php if ( $video_text !== '' ) : ?>
					<div class="video-text">
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</a>
