<?php
while ( $brook_query->have_posts() ) :
	$brook_query->the_post();
	$classes = array( 'portfolio-item grid-item swiper-slide' );
	?>
	<div <?php post_class( implode( ' ', $classes ) ); ?>>
		<div class="post-wrapper">
			<div class="post-thumbnail">
				<a href="<?php the_permalink(); ?>">
					<?php
					if ( has_post_thumbnail() ) { ?>
						<?php
						Brook_Image::the_post_thumbnail( array(
							'size'   => 'custom',
							'width'  => 9999,
							'height' => 550,
							'crop'   => false,
						) );
						?>
					<?php } else {
						Brook_Templates::image_placeholder( 600, 550 );
					}
					?>
				</a>
			</div>

			<?php if ( $overlay_style !== '' ) : ?>
				<?php get_template_part( 'loop/portfolio/overlay', $overlay_style ); ?>
			<?php endif; ?>

		</div>
	</div>
<?php endwhile; ?>
