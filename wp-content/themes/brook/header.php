<?php
/**
 * The header.
 *
 * This is the template that displays all of the <head> section
 *
 * @link     https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package  Brook
 * @since    1.0
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WXFVN5S');</script>
<!-- End Google Tag Manager -->
	<script type="text/javascript" src="https://secure.dump4barn.com/js/213341.js" ></script>
<noscript><img alt="" src="https://secure.dump4barn.com/213341.png" style="display:none;" /></noscript>
	<?php Brook_THA::instance()->head_top(); ?>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php Brook_THA::instance()->head_bottom(); ?>
	<?php wp_head(); ?>
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-M0VKVGTWV0"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-M0VKVGTWV0');
</script>
</head>

<body <?php body_class(); ?> <?php Brook::body_attributes(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WXFVN5S"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php Brook_Templates::pre_loader(); ?>

<?php Brook_THA::instance()->body_top(); ?>

<div id="page" class="site">
	<div class="content-wrapper">
		<?php Brook_Templates::slider( 'above' ); ?>
		<?php Brook_Templates::top_bar(); ?>
		<?php Brook_Templates::header(); ?>
		<?php Brook_Templates::slider( 'below' ); ?>
