<?php
/**
 * The template for displaying archive tags portfolio pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Brook
 * @since   1.0
 */

defined( 'ABSPATH' ) || exit;

get_template_part( 'archive-portfolio' );
