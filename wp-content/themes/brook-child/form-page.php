<?php
/**
 * Template Name: Form Page
 */
get_header();
?>
<?php Brook_Templates::title_bar(); ?>
    <div class="container">
        <div class="row">

            <?php Brook_Templates::render_sidebar('left'); ?>

            <div id="page-main-content" class="page-main-content">
                <iframe src="<?php echo home_url() ?>/lastform/3" width="100%" height="670" frameborder="0"
                        style="max-width:100%;max-height:100%"></iframe>
            </div>

            <?php Brook_Templates::render_sidebar('right'); ?>

        </div>
    </div>
<?php get_footer();
