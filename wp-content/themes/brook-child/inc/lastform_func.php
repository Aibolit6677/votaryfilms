<?php
/**
 * form fields
 */

add_action('gform_field_standard_settings', 'add_custom_settings', 20, 2);
function add_custom_settings($position, $form_id)
{

    //create settings on position 25 (right after Field Label)
    if ($position == 1600) {
        ?>
        <hr>
        <h3>Scopes:</h3>
        <li class="culture">
            <label for="culture" class="section_label">
                <?php esc_html_e('Culture', 'gravityforms'); ?>
                <?php gform_tooltip('culture') ?>
            </label>
            <input type="checkbox" id="culture"
                   onclick="SetFieldProperty('culture', this.checked);"/>
        </li>
        <li class="culture_vis">
            <label for="culture_vis" class="section_label">
                <?php esc_html_e('Culture Visibility', 'gravityforms'); ?>
                <?php gform_tooltip('culture_vis') ?>
            </label>
            <input type="checkbox" id="culture_vis"
                   onclick="SetFieldProperty('culture_vis', this.checked);"/>
        </li>
        <li class="operations">
            <label for="operations" class="section_label">
                <?php esc_html_e('Operations', 'gravityforms'); ?>
                <?php gform_tooltip('operations') ?>
            </label>
            <input type="checkbox" id="operations"
                   onclick="SetFieldProperty('operations', this.checked);"/>
        </li>
        <li class="operations_vis">
            <label for="operations_vis" class="section_label">
                <?php esc_html_e('Operations Visibility', 'gravityforms'); ?>
                <?php gform_tooltip('operations_vis') ?>
            </label>
            <input type="checkbox" id="operations_vis"
                   onclick="SetFieldProperty('operations_vis', this.checked);"/>
        </li>
        <li class="marketing">
            <label for="marketing" class="section_label">
                <?php esc_html_e('Marketing', 'gravityforms'); ?>
                <?php gform_tooltip('marketing') ?>
            </label>
            <input type="checkbox" id="marketing"
                   onclick="SetFieldProperty('marketing', this.checked);"/>
        </li>
        <li class="marketing_vis">
            <label for="marketing_vis" class="section_label">
                <?php esc_html_e('Marketing Visibility', 'gravityforms'); ?>
                <?php gform_tooltip('marketing_vis') ?>
            </label>
            <input type="checkbox" id="marketing_vis"
                   onclick="SetFieldProperty('marketing_vis', this.checked);"/>
        </li>


        <hr>
        <?php
    }
}

add_action('gform_editor_js', 'add_custom_settings_to_meta');
function add_custom_settings_to_meta()
{
    ?>
    <script type='text/javascript'>
        //adding setting to fields of type "text"
        fieldSettings.gquiz_setting_question += ', .culture';
        fieldSettings.culture += ', .culture_vis';
        fieldSettings.culture_vis += ', .operations';
        fieldSettings.operations += ', .operations_vis';
        fieldSettings.operations_vis += ', .marketing';
        fieldSettings.marketing += ', .marketing_vis';

        //binding to the load field settings event to initialize the checkbox
        jQuery(document).on('gform_load_field_settings', function (event, field, form) {
            jQuery('#culture').attr('checked', field.culture);
            jQuery('#culture_vis').attr('checked', field.culture_vis);
            jQuery('#operations').attr('checked', field.operations);
            jQuery('#operations_vis').attr('checked', field.operations_vis);
            jQuery('#marketing').attr('checked', field.marketing);
            jQuery('#marketing_vis').attr('checked', field.marketing_vis);
        });
    </script>
    <?php
}

add_filter('gform_tooltips', 'add_encryption_tooltips');
function add_encryption_tooltips($tooltips)
{
    $tooltips['culture'] = "<h6>Culture</h6>";
    $tooltips['culture_vis'] = "<h6>Culture Visibility</h6>";
    $tooltips['operations'] = "<h6>Operations</h6>";
    $tooltips['operations_vis'] = "<h6>Operations Visibility</h6>";
    $tooltips['marketing'] = "<h6>Marketing</h6>";
    $tooltips['marketing_vis'] = "<h6>Marketing Visibility</h6>";
    return $tooltips;
}

/**
 * meta boxes for pdf
 */

add_action('add_meta_boxes', 'add_pdf_meta');
function add_pdf_meta()
{
    global $post;
    if (!empty($post)) {
        $pageTemplate = get_post_meta($post->ID, '_wp_page_template', true);

        if ($pageTemplate == 'results-page.php') {
            add_meta_box('pdf_meta', 'PDF Meta fields', 'pdf_meta_callback', ['page']);
        }
    }
}

function pdf_meta_callback($post, $meta)
{
    $pdf_meta_top = get_post_meta($post->ID, 'pdf_meta_top', 1);
    $pdf_meta_bottom_left = get_post_meta($post->ID, 'pdf_meta_bottom_left', 1);
    $pdf_meta_bottom_right = get_post_meta($post->ID, 'pdf_meta_bottom_right', 1);
    ?>

    <div>
        <h4 for="pdf_meta_top"><?php echo __("Pdf Top", 'brook'); ?></h4>
        <?php
        wp_editor($pdf_meta_top, 'pdf_meta_top', array(
            'wpautop' => true,
            'media_buttons' => false,
            'textarea_name' => 'pdf_meta_top',
            'textarea_rows' => 10,
            'teeny' => false
        ));
        ?>
    </div>

    <div>
        <h4 for="pdf_meta_bottom_left"><?php echo __("Pdf Bottom Left", 'brook'); ?></h4>
        <?php
        wp_editor($pdf_meta_bottom_left, 'pdf_meta_bottom_left', array(
            'wpautop' => true,
            'media_buttons' => false,
            'textarea_name' => 'pdf_meta_bottom_left',
            'textarea_rows' => 10,
            'teeny' => false
        ));
        ?>
    </div>
    <div>
        <h4 for="pdf_meta_bottom_right"><?php echo __("Pdf Bottom Right", 'brook'); ?></h4>
        <?php
        wp_editor($pdf_meta_bottom_right, 'pdf_meta_bottom_right', array(
            'wpautop' => true,
            'media_buttons' => false,
            'textarea_name' => 'pdf_meta_bottom_right',
            'textarea_rows' => 10,
            'teeny' => false
        ));
        ?>
    </div>

    <?php
}

add_action('save_post', 'save_pdf_meta');
function save_pdf_meta($post_id)
{
    if (!isset($_POST['pdf_meta_top'])) {
        return;
    }

    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }

    if (!current_user_can('edit_post', $post_id)) {
        return;
    }

    update_post_meta($post_id, 'pdf_meta_top', $_POST['pdf_meta_top']);
    update_post_meta($post_id, 'pdf_meta_bottom_left', $_POST['pdf_meta_bottom_left']);
    update_post_meta($post_id, 'pdf_meta_bottom_right', $_POST['pdf_meta_bottom_right']);
}

/**
 * Add sizes to the "Font Size" drop-down in wiziwig.
 */

if (!function_exists('wpex_mce_buttons')) {
    function wpex_mce_buttons($buttons)
    {
        array_unshift($buttons, 'fontselect'); // Add Font Select
        array_unshift($buttons, 'fontsizeselect'); // Add Font Size Select
        return $buttons;
    }
}
add_filter('mce_buttons_2', 'wpex_mce_buttons');

if (!function_exists('wpex_mce_text_sizes')) {
    function wpex_mce_text_sizes($initArray)
    {
        $initArray['fontsize_formats'] = "9px 10px 12px 13px 14px 16px 18px 21px 24px 26px 28px 32px 36px 40px 50px 56px";
        return $initArray;
    }
}
add_filter('tiny_mce_before_init', 'wpex_mce_text_sizes');

/**
 * Add fonts to the "Font Family" drop-down in wiziwig.
 */
add_filter('tiny_mce_before_init', 'fb_mce_before_init');

function fb_mce_before_init($settings)
{

    $font_formats = 'Moderat Bold=moderatbold,sans-serif;'
        . 'Moderat Medium=moderatmedium,sans-serif;'
        . 'Moderat Light=moderatlight,sans-serif;'
        . 'MaisonNeue-Bold=MaisonNeue-Bold,sans-serif;'
        . 'MaisonNeue=MaisonNeue,sans-serif;';
    $settings['font_formats'] = $font_formats;

    return $settings;

}

add_action('wp_enqueue_scripts', 'results_scripts');
function results_scripts()
{
    wp_enqueue_style('results-style', get_stylesheet_directory_uri() . '/assets/css/results.css?v=' . time());
}

/**
 * percent coordinates
 * $flag - "small" or "large"
 * $percent - numeric, from 1 to 100
 */

function percent_coordinates($flag, $percent)
{
    $deg = round(270 / 100 * $percent, 0) - 90;
    if ($flag == 'large') {
        $r = 280;
    } else {
        $r = 180;
    }
    $x = round(450 + $r * cos(deg2rad($deg)), 0);
    $y = round(300 + $r * sin(deg2rad($deg)), 0);
    return [$x, $y];
}


