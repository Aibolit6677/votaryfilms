<?php
/**
 * Template Name: Create PDF
 */
global $wp_query;
if ( !isset($_GET['rrr']) ) {
    $wp_query->set_404();
    status_header( 404 );
    get_template_part( 404 );
    exit();
}
define('PATH_TO_THEME', get_stylesheet_directory_uri());

$entry_id = $_GET['rrr'];
$result = GFAPI::get_entry($entry_id);
$form = GFAPI::get_form($result['form_id']);

$scopes_types = array(
    'culture',
    'culture_vis',
    'operations',
    'operations_vis',
    'marketing',
    'marketing_vis',
);
$scopes = [];
$scopes_result = [];
$max_values = [];

foreach ($form['fields'] as $field) {
    foreach ($scopes_types as $type) {
        if (key_exists($type, $field) and $field[$type] == '1') {
            $scopes[$field['id']][] = $type;
            $max_values[$type] +=5;
        }
    }
}
foreach ($result as $answer => $value) {
    if (key_exists($answer, $scopes)) {
        foreach ($scopes[$answer] as $scope) {
            $scopes_result[$scope][] = $value;
        }
    }
}
$test_results = ['MARKETING' => [], 'OPERATIONS' => [], 'CULTURE' => []];
$culture = array_sum($scopes_result['culture']);
$test_results['CULTURE']['item'] = round($culture / $max_values['culture'], 2) * 100;

$culture_vis = array_sum($scopes_result['culture_vis']);
$test_results['CULTURE']['vis'] = round($culture_vis / $max_values['culture_vis'], 2) * 100;

$operations = array_sum($scopes_result['operations']);
$test_results['OPERATIONS']['item'] = round($operations / $max_values['operations'], 2) * 100;

$operations_vis = array_sum($scopes_result['operations_vis']);
$test_results['OPERATIONS']['vis'] = round($operations_vis / $max_values['operations_vis'], 2) * 100;

$marketing = array_sum($scopes_result['marketing']);
$test_results['MARKETING']['item'] = round($marketing / $max_values['marketing'], 2) * 100;

$marketing_vis = array_sum($scopes_result['marketing_vis']);
$test_results['MARKETING']['vis'] = round($marketing_vis / $max_values['marketing_vis'], 2) * 100;


ob_start();
?>
    <html>
    <body>
    <header style="margin-top: 20px;margin-bottom: 0px">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png"
             style="width: 30%; float: right;margin-right: 40px;margin-top: 10pt;">
    </header>
    <div id="page-main-content">
        <div class="results-top-block wm"
             style="padding-left: 60px;padding-right: 60px;margin-bottom: 50px;font-size: 14px">
            <?php
            $tmp_res = preg_replace('/\sstyle=("|\').*?("|\')/i', '', get_post_meta($_GET['ppp'], 'pdf_meta_top', true));
            echo str_replace('{{company_name}}',$result[33],$tmp_res);
            ?>
        </div>
        <div class="results-diagram">
            <div class="results-diagram-wrap wm">
                <?php
                $counter = 0;
                foreach ($test_results as $type => $result):
                    $more_half_flag_item = 0;
                    $more_half_flag_vis = 0;
                    if ($result['item'] > 67) {
                        $more_half_flag_item = 1;
                    }
                    if ($result['vis'] > 67) {
                        $more_half_flag_vis = 1;
                    }
                    $coord_item = percent_coordinates('large', $result['item']);
                    $coord_vis = percent_coordinates('small', $result['vis']);
                    switch ($counter) {
                        case 0:
                            $x='110';
                            break;
                        case 1:
                            $x='80';
                            break;
                        case 2:
                            $x='170';
                            break;
                    }
                    ?>
                    <div class="diagram-item">
                        <svg width="100%" height="100%" viewBox="0 0 900 600"
                             xmlns:xlink="http://www.w3.org/1999/xlink"
                             class="diagram-svg" font-weight="400">
                            <g class="chart-text type-title">
                                <text x="<?php echo $x;?>px" y="42px" class="chart-number" stroke="white"
                                      fill="white" font-size="54px">
                                    <?php echo $type; ?>
                                </text>
                            </g>
                            <g class="chart-text">
                                <text x="190px" y="135px" class="chart-number" stroke="#c2a177"
                                      fill="#c2a177" font-size="46px"
                                      font-weight="400">
                                    VISIBILITY
                                </text>
                            </g>
                            <g class="chart-text">
                                <text x="<?php echo $result['vis']==100?'330':'350';?>px" y="335px" class="chart-number" stroke="#c2a177"
                                      fill="#c2a177" letter-spacing="0"
                                      font-weight="100">
                                    <tspan font-size="106px"
                                           font-weight="400"><?php echo $result['vis']; ?></tspan>
                                    <tspan font-size="64px" font-weight="400">%</tspan>
                                </text>
                            </g>
                            <g class="chart-text">
                                <text x="<?php echo $result['item']==100?'636':'680';?>px" y="598px" class="chart-number" stroke="white"
                                      fill="white" letter-spacing="0"
                                      font-weight="400">
                                    <tspan font-size="106px"><?php echo $result['item']; ?></tspan>
                                    <tspan font-size="64px">%</tspan>
                                </text>
                            </g>

                            <path d="M 450 10 A 290 290, 0, 1, 1, 160 290"
                                  stroke="white" fill="transparent" stroke-width="20"
                                  stroke-linecap="round" stroke-opacity="0.5"/>
                            <path d="M 450 20 A 280 280, 0, <?php echo $more_half_flag_item . ', 1, ' . $coord_item[0] . ' ' . $coord_item[1]; ?>"
                                  stroke="white" fill="transparent" stroke-width="40"
                                  stroke-linecap="round" stroke-opacity="1"/>

                            <path d="M 450 110 A 190 190, 0, 1, 1, 260 290"
                                  stroke="#c2a177" fill="transparent" stroke-width="20"
                                  stroke-linecap="round" stroke-opacity="0.5"/>
                            <path d="M 450 120 A 180 180, 0, <?php echo $more_half_flag_vis . ', 1, ' . $coord_vis[0] . ' ' . $coord_vis[1]; ?>"
                                  stroke="#c2a177" fill="transparent" stroke-width="40"
                                  stroke-linecap="round" stroke-opacity="1"/>
                        </svg>
                    </div>
                    <?php
                    $counter++;
                endforeach; ?>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="results-bottom wm" style="padding-left: 60px;padding-right: 0;margin-top: 30px;">
            <div class="results-bottom-div results-bottom-left">
                <?php echo preg_replace('/\sstyle=("|\').*?("|\')/i', '', apply_filters('the_content', get_post_meta($_GET['ppp'], 'pdf_meta_bottom_left', true))); ?>
            </div>
            <div class="results-bottom-div results-bottom-right">
                <?php
                echo preg_replace('/\sstyle=("|\').*?("|\')/i', '', get_post_meta($_GET['ppp'], 'pdf_meta_bottom_right', true));
                ?>
            </div>
        </div>
    </div>
    </body>
    </html>
<?php
$content = ob_get_contents();
ob_end_clean();
//echo $content;die;
require_once __DIR__ . '/inc/vendor/autoload.php';
$defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
$fontDirs = $defaultConfig['fontDir'];

$defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
$fontData = $defaultFontConfig['fontdata'];

$mpdf = new \Mpdf\Mpdf([
    'mode' => 'utf-8',
    'fontDir' => array_merge($fontDirs, [
        __DIR__ . '/fonts',
    ]),
    'fontdata' => $fontData + [
            'moderat' => [
                'moderat_bold' => 'moderat-bold-webfont.ttf',
                'moderat_light' => 'moderat-light-webfont.ttf',
            ],
            'maison_neue' => [
                'maison_neue_book' => 'maison_neue_book-webfont.ttf',
                'maison_neue_demi' => 'maison_neue_demi-webfont.ttf',
            ],
        ],
    'default_font' => 'maison_neue'
]);
$stylesheet = file_get_contents(get_stylesheet_directory_uri() . '/assets/css/results_pdf.css');
$mpdf->SetHTMLHeader();
$mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS);
$mpdf->WriteHTML($content, \Mpdf\HTMLParserMode::HTML_BODY);
$mpdf->SetHTMLFooter('<footer><span>© 2019 Votary Films. All rights reserved.</span></footer>');
//$mpdf->Output();
$mpdf->Output('result.pdf',\Mpdf\Output\Destination::DOWNLOAD);
