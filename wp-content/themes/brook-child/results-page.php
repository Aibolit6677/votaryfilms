<?php

/**
 * Template Name: Results Page
 */
ini_set('display_errors', 1);
global $wp_query;
if (!isset($_GET['result'])) {
    $wp_query->set_404();
    status_header(404);
    get_template_part(404);
    exit();
}
get_header();
include_once 'wp-content/plugins/gf-hubspot/gf-hubspot.php';
include_once 'wp-content/plugins/gf-hubspot/api/api.php';

class customHubspotApi extends vxg_hubspot_api
{

}

?>
<?php Brook_Templates::title_bar(); ?>
    <div id="page-content" class="results-page-content">
        <div class="row">
            <?php Brook_Templates::render_sidebar('left'); ?>
            <div id="page-main-content" class="page-main-content">
                <?php if (have_posts()) : the_post();
                    if (!empty($_GET['result'])) {
                        $entry_id = $_GET['result'];
                        $result = GFAPI::get_entry($entry_id);
                    }
                    if (is_array($result)):
                        $form = GFAPI::get_form($result['form_id']);
                        $scopes_types = array(
                            'culture',
                            'culture_vis',
                            'operations',
                            'operations_vis',
                            'marketing',
                            'marketing_vis',
                        );
                        $scopes = [];
                        $scopes_result = [];
                        $max_values = [];
                        $form_for_hubspot = [];
                        $result_for_hubspot = [];

                        foreach ($form['fields'] as $field) {
                            foreach ($scopes_types as $type) {
                                if (key_exists($type, $field) and $field[$type] == '1') {
                                    $scopes[$field['id']][] = $type;
                                    $max_values[$type] += 5;
                                }
                            }
                            $tmp_arr = [];
                            $choices = [];
                            if (is_array($field['choices'])) {
                                foreach ($field['choices'] as $choice) {
                                    $choices[$choice['value']] = $choice['text'];
                                }
                            }

                            $tmp_arr = [
                                'label' => $field['label'],
                                'choices' => $choices
                            ];
                            $form_for_hubspot[$field['id']] = $tmp_arr;
                        }
                        foreach ($result as $answer => $value) {
                            if (key_exists($answer, $scopes)) {
                                foreach ($scopes[$answer] as $scope) {
                                    $scopes_result[$scope][] = $value;
                                }
                            }
                            if (key_exists($answer, $form_for_hubspot)) {
                                $tmp_answer = '';
                                if (!empty($form_for_hubspot[$answer]['choices'])) {
                                    $tmp_answer = $form_for_hubspot[$answer]['choices'][$value];
                                } else {
                                    $tmp_answer = $value;
                                }
                                if ($answer < 32) {
                                    $result_for_hubspot[] = [
                                        'question' => $form_for_hubspot[$answer]['label'],
                                        'answer' => $tmp_answer
                                    ];
                                }

                            }
                        }
                        $entry_text = '';
                        foreach ($result_for_hubspot as $value) {
                            $entry_text .= 'Question: ' . $value['question'] . PHP_EOL . 'Answer: ' . $value['answer'] . PHP_EOL;
                        }
                        $test_results = ['marketing' => [], 'operations' => [], 'culture' => []];
                        $culture = array_sum($scopes_result['culture']);
                        $test_results['culture']['item'] = round($culture / $max_values['culture'], 2) * 100;

                        $culture_vis = array_sum($scopes_result['culture_vis']);
                        $test_results['culture']['vis'] = round($culture_vis / $max_values['culture_vis'], 2) * 100;

                        $operations = array_sum($scopes_result['operations']);
                        $test_results['operations']['item'] = round($operations / $max_values['operations'], 2) * 100;

                        $operations_vis = array_sum($scopes_result['operations_vis']);
                        $test_results['operations']['vis'] = round($operations_vis / $max_values['operations_vis'], 2) * 100;

                        $marketing = array_sum($scopes_result['marketing']);
                        $test_results['marketing']['item'] = round($marketing / $max_values['marketing'], 2) * 100;

                        $marketing_vis = array_sum($scopes_result['marketing_vis']);
                        $test_results['marketing']['vis'] = round($marketing_vis / $max_values['marketing_vis'], 2) * 100;
                        if (!empty($result[35])) {
                            $handle = curl_init();

                            $url = "https://api.hubapi.com/contacts/v1/contact/email/" . $result[35] . "/profile?hapikey=5c4fabc2-d611-44b9-b4d1-61e6fd3bd4f6";

                            $results_test['marketing']['item'] = 'Marketing ' . $test_results['marketing']['item'] . '%';
                            $results_test['marketing']['vis'] = 'Visibility  ' . $test_results['marketing']['vis'] . '%';

                            $results_test['operations']['item'] = 'Operations  ' . $test_results['operations']['item'] . '%';
                            $results_test['operations']['vis'] = 'Visibility  ' . $test_results['operations']['vis'] . '%';

                            $results_test['culture']['item'] = 'Culture  ' . $test_results['culture']['item'] . '%';
                            $results_test['culture']['vis'] = 'Visibility  ' . $test_results['culture']['vis'] . '%';

                            $results_test = $results_test['marketing']['item'] . PHP_EOL . $results_test['marketing']['vis'] . PHP_EOL . 
                            $results_test['operations']['item'] . PHP_EOL . $results_test['operations']['vis'] . PHP_EOL .
                            $results_test['culture']['item'] . PHP_EOL . $results_test['culture']['vis'];

                            $post_data = [
                                'properties' => [[
                                    'property' => 'forms_results',
                                    'value' => $entry_text . $results_test,

                                ]]
                            ];

                            curl_setopt_array($handle,
                                array(
                                    CURLOPT_URL => $url,
                                    CURLOPT_RETURNTRANSFER => true,
                                    CURLOPT_POST => 1,
                                    CURLOPT_POSTFIELDS => json_encode($post_data),
                                    CURLOPT_HTTPHEADER => array(
                                        'Content-Type: application/json',
                                    ),
                                )
                            );
                            $output = curl_exec($handle);
                            curl_close($handle);
                        }
                        ?>
                        <?php if ($_SERVER['HTTP_REFERER'] == 'https://www.votaryfilms.com/lastform/3') : ?>
                            <meta http-equiv="refresh"
                              content="1;URL=<?php echo home_url() . "/create-pdf?rrr=" . $entry_id . '&ppp=' . $post->ID; ?>"/>
                        <?php endif; ?>
                        <div class="results-top-block wm">
                            <a href="<?php echo home_url() . "/create-pdf?rrr=" . $entry_id . '&ppp=' . $post->ID; ?>"
                               class="download-pdf">Download Pdf</a>
                            <?php echo str_replace('{{company_name}}', $result[32], apply_filters('the_content', get_post_meta($post->ID, 'pdf_meta_top', true))); ?>
                        </div>
                        <div class="results-diagram">
                            <div class="results-diagram-wrap wm">
                                <?php foreach ($test_results as $type => $result):
                                    $more_half_flag_item = 0;
                                    $more_half_flag_vis = 0;
                                    if ($result['item'] > 67) {
                                        $more_half_flag_item = 1;
                                    }
                                    if ($result['vis'] > 67) {
                                        $more_half_flag_vis = 1;
                                    }
                                    $coord_item = percent_coordinates('large', $result['item']);
                                    $coord_vis = percent_coordinates('small', $result['vis']);
                                    ?>
                                    <div class="diagram-item">
                                        <svg width="100%" height="100%" viewBox="0 0 900 720"
                                             xmlns:xlink="http://www.w3.org/1999/xlink"
                                             class="diagram-svg" font-weight="400">
                                            <g class="chart-text type-title">
                                                <text x="40px" y="42px" class="chart-number" stroke="white"
                                                      fill="white" font-size="54px">
                                                    HEALTH
                                                </text>
                                            </g>
                                            <g class="chart-text">
                                                <text x="126px" y="135px" class="chart-number" stroke="#c2a177"
                                                      fill="#c2a177" font-size="46px"
                                                      font-weight="400">
                                                    VISIBILITY
                                                </text>
                                            </g>
                                            <g class="chart-text">
                                                <text x="<?php echo $result['vis'] == 100 ? '330' : '350'; ?>px"
                                                      y="335px" class="chart-number" stroke="#c2a177"
                                                      fill="#c2a177" letter-spacing="0"
                                                      font-weight="100">
                                                    <tspan font-size="106px"
                                                           font-weight="400"><?php echo $result['vis']; ?></tspan>
                                                    <tspan font-size="64px" font-weight="400">%</tspan>
                                                </text>
                                            </g>
                                            <g class="chart-text">
                                                <text x="<?php echo $result['item'] == 100 ? '636' : '680'; ?>px"
                                                      y="598px" class="chart-number" stroke="white"
                                                      fill="white" letter-spacing="0"
                                                      font-weight="400">
                                                    <tspan font-size="106px"><?php echo $result['item']; ?></tspan>
                                                    <tspan font-size="64px">%</tspan>
                                                </text>
                                            </g>
                                            <g class="title-text">
                                                <text x="<?php echo $type == 100 ? '230' : '230'; ?>px"
                                                      y="720px" class="chart-number" stroke="white"
                                                      fill="white" letter-spacing="0"
                                                      font-weight="400">
                                                  
                                                    <tspan font-size="64px"><?php echo $type; ?></tspan>
                                                </text>
                                            </g>

                                            <path d="M 450 10 A 290 290, 0, 1, 1, 160 290"
                                                  stroke="white" fill="transparent" stroke-width="20"
                                                  stroke-linecap="round" stroke-opacity="0.5"/>
                                            <path d="M 450 20 A 280 280, 0, <?php echo $more_half_flag_item . ', 1, ' . $coord_item[0] . ' ' . $coord_item[1]; ?>"
                                                  stroke="white" fill="transparent" stroke-width="40"
                                                  stroke-linecap="round" stroke-opacity="1"/>

                                            <path d="M 450 110 A 190 190, 0, 1, 1, 260 290"
                                                  stroke="#c2a177" fill="transparent" stroke-width="20"
                                                  stroke-linecap="round" stroke-opacity="0.5"/>
                                            <path d="M 450 120 A 180 180, 0, <?php echo $more_half_flag_vis . ', 1, ' . $coord_vis[0] . ' ' . $coord_vis[1]; ?>"
                                                  stroke="#c2a177" fill="transparent" stroke-width="40"
                                                  stroke-linecap="round" stroke-opacity="1"/>
                                        </svg>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="results-bottom wm">
                            <div class="results-bottom-div results-bottom-left">
                                <?php echo apply_filters('the_content', get_post_meta($post->ID, 'pdf_meta_bottom_left', true)); ?>
                            </div>
                            <div class="results-bottom-div results-bottom-right">
                                <?php echo apply_filters('the_content', get_post_meta($post->ID, 'pdf_meta_bottom_right', true)); ?>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    <?php else: ?>
                        <h2>No results found</h2>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
            <?php Brook_Templates::render_sidebar('right'); ?>
        </div>
    </div>
<?php get_footer(); ?>