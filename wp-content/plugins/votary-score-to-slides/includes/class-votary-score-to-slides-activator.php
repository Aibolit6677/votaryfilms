<?php

/**
 * Fired during plugin activation
 *
 * @link       https://votaryfilms.com/
 * @since      1.0.0
 *
 * @package    Votary_Score_To_Slides
 * @subpackage Votary_Score_To_Slides/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Votary_Score_To_Slides
 * @subpackage Votary_Score_To_Slides/includes
 * @author     Boris Volkovich <greenmoonbase@gmail.com>
 */
class Votary_Score_To_Slides_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
