<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://votaryfilms.com/
 * @since      1.0.0
 *
 * @package    Votary_Score_To_Slides
 * @subpackage Votary_Score_To_Slides/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Votary_Score_To_Slides
 * @subpackage Votary_Score_To_Slides/includes
 * @author     Boris Volkovich <greenmoonbase@gmail.com>
 */
class Votary_Score_To_Slides_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
