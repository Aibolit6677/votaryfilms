<?php

/** Plugin Google API handle */

/** Google API Class
 *
 * Used to do everything regarding Google API
 *
 * @since 1.0.0
 * @package Votary_Score_To_Slides
 * @subpackage Votary_Score_To_Slides/includes
 * @author     Boris Volkovich <greenmoonbase@gmail.com>
 */
 class Votary_Score_To_Slides_Google_API {
     public function __construct(){
         $this->init();
     }

     public function init(){
         // Get the API client and construct the service object.
         $client = $this->getClient();
         $service = new Google_Service_Slides($client);

        // Prints the number of slides and elements in a sample presentation:
        // https://docs.google.com/presentation/d/1EAYk18WDjIG-zp_0vLm3CsfQh_i8eXc67Jo2O9C6Vuc/edit
         $presentationId = '1EAYk18WDjIG-zp_0vLm3CsfQh_i8eXc67Jo2O9C6Vuc';
         $presentation = $service->presentations->get($presentationId);
         $slides = $presentation->getSlides();

         printf("The presentation contains %s slides:\n", count($slides));
         foreach ($slides as $i => $slide) {
             // Print columns A and E, which correspond to indices 0 and 4.
             printf("- Slide #%s contains %s elements.\n", $i + 1,
                 count($slide->getPageElements()));
         }
     }

     /**
      * Returns an authorized API client.
      * @return Google_Client the authorized client object
      */
     public function getClient() {
         $client = new Google_Client();
         $client->setApplicationName('Google Slides API PHP Quickstart');
         $client->setScopes(Google_Service_Slides::PRESENTATIONS_READONLY);
         $client->setAuthConfig('credentials.json');
         $client->setAccessType('offline');
         $client->setPrompt('select_account consent');

         // Load previously authorized token from a file, if it exists.
         // The file token.json stores the user's access and refresh tokens, and is
         // created automatically when the authorization flow completes for (the first
         // time.
         $tokenPath = 'token.json';
         if (file_exists($tokenPath)) {
             $accessToken = json_decode(file_get_contents($tokenPath), true);
             $client->setAccessToken($accessToken);
         }

         // If there is no previous token or it's expired.
         if ($client->isAccessTokenExpired()) {
             // Refresh the token if possible, else fetch a new one.
             if ($client->getRefreshToken()) {
                 $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
             } else {
                 // Request authorization from the user.
                 $authUrl = $client->createAuthUrl();
                 printf("Open the following link in your browser:\n%s\n", $authUrl);
                 print 'Enter verification code: ';
                 $authCode = trim(fgets(STDIN));

                 // Exchange authorization code for an access token.
                 $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
                 $client->setAccessToken($accessToken);

                 // Check to see if there was an error.
                 if (array_key_exists('error', $accessToken)) {
                     throw new Exception(join(', ', $accessToken));
                 }
             }
             // Save the token to a file.
             if (!file_exists(dirname($tokenPath))) {
                 mkdir(dirname($tokenPath), 0700, true);
             }
             file_put_contents($tokenPath, json_encode($client->getAccessToken()));
         }
         return $client;
     }
 }