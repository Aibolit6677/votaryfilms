<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://votaryfilms.com/
 * @since      1.0.0
 *
 * @package    Votary_Score_To_Slides
 * @subpackage Votary_Score_To_Slides/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Votary_Score_To_Slides
 * @subpackage Votary_Score_To_Slides/includes
 * @author     Boris Volkovich <greenmoonbase@gmail.com>
 */
class Votary_Score_To_Slides_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'votary-score-to-slides',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
