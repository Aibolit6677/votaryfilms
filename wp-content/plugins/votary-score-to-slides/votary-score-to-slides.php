<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://votaryfilms.com/
 * @since             1.0.0
 * @package           Votary_Score_To_Slides
 *
 * @wordpress-plugin
 * Plugin Name:       Votary Score To Slides
 * Plugin URI:        https://votaryfilms.com/
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Boris Volkovich
 * Author URI:        https://votaryfilms.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       votary-score-to-slides
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'VOTARY_SCORE_TO_SLIDES_VERSION', '1.0.0' );

/**
 * load autoloader if function not yet exists (for compat with sitewide autoloader)
 */
if ( ! function_exists( 'mc4wp' ) ) {
    require_once plugin_dir_path( __FILE__ ) . '/vendor/autoload.php';
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-votary-score-to-slides-activator.php
 */
function activate_votary_score_to_slides() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-votary-score-to-slides-activator.php';
	Votary_Score_To_Slides_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-votary-score-to-slides-deactivator.php
 */
function deactivate_votary_score_to_slides() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-votary-score-to-slides-deactivator.php';
	Votary_Score_To_Slides_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_votary_score_to_slides' );
register_deactivation_hook( __FILE__, 'deactivate_votary_score_to_slides' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-votary-score-to-slides.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_votary_score_to_slides() {

	$plugin = new Votary_Score_To_Slides();
	$plugin->run();

}
run_votary_score_to_slides();
