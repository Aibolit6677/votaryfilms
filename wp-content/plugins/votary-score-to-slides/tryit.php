<?php
require __DIR__ . '/vendor/autoload.php';

if (php_sapi_name() != 'cli') {
    throw new Exception('This application must be run on the command line.');
}

/**
 * Returns an authorized API client.
 * @return Google_Client the authorized client object
 */
function getClient()
{
    $client = new Google_Client();
    $client->setApplicationName('Slides for Wordpress');
    $client->addScope(Google\Service\Drive::DRIVE);
    $client->addScope(Google_Service_Slides::PRESENTATIONS);
    $client->setAuthConfig('client_secret_141598822231-3m9p5a718sjqse2s4k7ujj5rudv8fdlp.apps.googleusercontent.com.json');

    $client->setAccessType('offline');
    $client->setPrompt('select_account consent');

    // Load previously authorized token from a file, if it exists.
    // The file token.json stores the user's access and refresh tokens, and is
    // created automatically when the authorization flow completes for the first
    // time.
    $tokenPath = 'tryittoken.json';
    if (file_exists($tokenPath)) {
        $accessToken = json_decode(file_get_contents($tokenPath), true);
        $client->setAccessToken($accessToken);
    }

    // If there is no previous token or it's expired.
    if ($client->isAccessTokenExpired()) {
        // Refresh the token if possible, else fetch a new one.
        if ($client->getRefreshToken()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        } else {
            $authUrl = $client->createAuthUrl();
            printf("Open the following link in your browser:\n%s\n", $authUrl);
            print 'Enter verification code: ';
            $authCode = trim(fgets(STDIN));

            // Exchange authorization code for an access token.
            $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
            $client->setAccessToken($accessToken);

            // Check to see if there was an error.
            if (array_key_exists('error', $accessToken)) {
                throw new Exception(join(', ', $accessToken));
            }
        }
        // Save the token to a file.
        if (!file_exists(dirname($tokenPath))) {
            mkdir(dirname($tokenPath), 0700, true);
        }
        file_put_contents($tokenPath, json_encode($client->getAccessToken()));
    }
    return $client;
}


//// Get the API client and construct the service object.
$client = getClient();

$driveService = new Google_Service_Drive($client);

$optParams = array(
    'q'=> 'name = "Slides" and "root" in parents and trashed = false',
    'pageSize' => 10,
    'fields' => 'nextPageToken, files(id, name)'
);

$results = $driveService->files->listFiles($optParams);

$slidesFolder = false;

if (count($results->getFiles()) == 0) {

    $folder = new Google_Service_Drive_DriveFile();
    $folder->setName('Slides');
    $folder->setMimeType('application/vnd.google-apps.folder');

    $slidesFolder = $driveService->files->create($folder);
} else {
    $slidesFolder = $results->getFiles()[0];
}

function clonePresentationByName(Google_Service_Drive $driveService, $copyName)
{
    $response = $driveService->files->listFiles([
        'q' => "mimeType='application/vnd.google-apps.presentation' and name='Acceleration Workshop Presentation Template tests'",
        'spaces' => 'drive',
        'fields' => 'files(id, name)',
    ]);
    if($response->files){
        $templatePresentationId = $response->files[0]->id;
    } else {
        throw new Exception('Template presentation not found');
    }

    $copy = new Google_Service_Drive_DriveFile([
        'name' => $copyName
    ]);
    $driveResponse = $driveService->files->copy($templatePresentationId, $copy);

    return $driveResponse->id;
}



$presentationFile = new Google_Service_Drive_DriveFile();
$presentationFile->setName('Presentation');
$presentationFile->setMimeType('application/vnd.google-apps.presentation');
$presentationFile->setParents([$slidesFolder->getId()]);

$presentationFile = $driveService->files->create($presentationFile);

$slidesService = new Google_Service_Slides($client);
$driveService = new Google_Service_Drive($client);

$presentation = $slidesService->presentations->get($presentationFile->getId());

$pname = "new presentation 24.11.2021";



$test = clonePresentationByName($driveService, $pname);

    // Create the text merge (replaceAllText) requests for this presentation.
    $requests = array();
    $requests[] = new Google_Service_Slides_Request(array(
        'replaceAllText' => array(
            'containsText' => array(
                'text' => '{{test}}',
                'matchCase' => true
            ),
            'replaceText' => 'Rostislav'
        )
    ));
    $requests[] = new Google_Service_Slides_Request(array(
        'deleteText' => array(
            'objectId' => 'gf24ca6a1cc_0_40',
            'cellLocation' => array(
                'rowIndex' => '2',
                'columnIndex' => '1'
            ),
            'textRange' => array(
            'type' => 'ALL',
            )
        ),
        
    ));
    $requests[] = new Google_Service_Slides_Request(array(
        'insertText' => array(
            'objectId' => 'gf24ca6a1cc_0_40',
            'cellLocation' => array(
                'rowIndex' => '2',
                'columnIndex' => '1'
            ),
            'text' => 'Kangaroo',
            'insertionIndex' => 0
        ),
        
    ));

    // Execute the requests for this presentation.
    $batchUpdateRequest = new Google_Service_Slides_BatchUpdatePresentationRequest(array(
        'requests' => $requests
    ));
    $response =
        $slidesService->presentations->batchUpdate($test, $batchUpdateRequest);

printf($presentation->presentationId);


//$slidesService = new Google_Service_Slides($client);
//
//$presentation = new Google_Service_Slides_Presentation([
//    'title' => 'Presentation'
//]);
//
//$presentation = $slidesService->presentations->create($presentation);
//$presentationFile = $driveService->files->get($presentation->getPresentationId(),[
//    'fields' => 'id, name, parents'
//]);
//$presentationFile->setParents([$slidesFolder->getId()]);
//
//
//$presentationFileNew = $driveService->files->copy($presentationFile->getId(), $presentationFile);
//$driveService->files->delete($presentationFile);
//


//
//
////List files
//$optParams = array(
//    'q'=> 'name = "Slides" and "root" in parents and trashed = false',
//    'pageSize' => 10,
//    'fields' => 'nextPageToken, files(id, name)'
//);
//
//$results = $driveService->files->listFiles($optParams);
//
//if (count($results->getFiles()) == 0) {
//    print "No files found.\n";
//} else {
//    print "Files:\n";
//    foreach ($results->getFiles() as $file) {
//        printf("%s (%s)\n", $file->getName(), $file->getId());
//    }
//}