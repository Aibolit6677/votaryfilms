<?php

require  plugin_dir_path( dirname( __FILE__ ) ) . '/vendor/autoload.php';
/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://votaryfilms.com/
 * @since      1.0.0
 *
 * @package    Votary_Score_To_Slides
 * @subpackage Votary_Score_To_Slides/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Votary_Score_To_Slides
 * @subpackage Votary_Score_To_Slides/admin
 * @author     Boris Volkovich <greenmoonbase@gmail.com>
 */
class Votary_Score_To_Slides_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Votary_Score_To_Slides_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Votary_Score_To_Slides_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/votary-score-to-slides-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Votary_Score_To_Slides_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Votary_Score_To_Slides_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/votary-score-to-slides-admin.js', array( 'jquery' ), $this->version, false );

	}
	
    /**
     * Register Menu Pages
     *
     * @since 1.0.0
     */
	 
    public function menu_pages() {
        $hook = add_menu_page(
            'Score To Slides',
            'Score To Slides',
            'manage_options',
            __CLASS__, 
			array($this, 'createTable'),
            '',
            85
        );
		add_action('load-'.$hook, array($this, 'showScreenOptions'));
		
        add_submenu_page(
            $this->plugin_name . '-menu',
            'General',
            'General',
            'manage_options',
            $this->plugin_name . '-menu',
            array($this, 'general_page'),
			
        );
		
    }
	
	public function setScreenOption($status, $option, $value)
	{
		if('plance_per_page' == $option )
		{
			return $value;
		}

		return $status;
	}
	
	public function showScreenOptions()
	{
		$this -> _Table = new Votary_Score_To_Slides_Admin_Table();
	}

    public function general_page() {
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/general.php';
    }
	
	public function createTable()
    {
        $this -> _Table -> prepare_items();
        ?>
            <div class="wrap">
                <h2><?php echo __('Votary Score To Slides', 'plance')?></h2>
				<form method="post">
					<input type="hidden" name="page" value="<?php echo __CLASS__ ?>" />
				
					<?php $this -> _Table -> display(); ?>
				</form>
            </div>
        <?php
    }
}

class Votary_Score_To_Slides_Admin_Table extends WP_List_Table
{
	/**
	 * The current filter e.g. trash, spam, unread
	 *
	 * @var string
	 */
	public $filter = '';

	/**
	 * The name of the primary column. The primary column will not get collapsed on narrower displays.
	 *
	 * @var null|string
	 */
	public $primary_column_name = null;

	/**
	 * The locking mechanism for the entry list.
	 *
	 * @var GFEntryLocking
	 */
	public $locking_info;

	/**
	 * Tracks the cuurent row during output.
	 *
	 * @var int
	 */
	public $row_index = 0;

	/**
	 * The Form array.
	 *
	 * @var array
	 */
	private $_form;

	/**
	 * The columns to display on the entry list for this form.
	 * @var array
	 */
	public function get_form_id() {
		$form_id = 3;
		return absint( $form_id );
	}
	
	 public function __construct( $args = array() ) {
		$this->_form = isset( $args['form'] ) ?  $args['form'] : null;
		if ( ! isset( $this->_form ) ) {
			$form_id = isset( $args['form_id'] ) ? $args['form_id'] : absint( rgget( 'id' ) );

			$this->_form = RGFormsModel::get_form_meta( $form_id );
		}

		$args = wp_parse_args( $args, array(
			'plural' => 'gf_entries',
			'singular' => 'gf_entry',
			'ajax' => false,
			'screen' => null,
			'filter' => sanitize_text_field( rgget( 'filter' ) ),
		) );

		parent::__construct( $args );
		$this->filter = $args['filter'];

		$this->set_columns();

		$this->locking_info = new GFEntryLocking();
	}
	
	public function set_columns() {
		$columns               = $this->get_columns();
		$hidden                = array();
		$sortable              = $this->get_sortable_columns();
		$primary               = $this->get_primary_column_name();
		$this->_column_headers = array( $columns, $hidden, $sortable, $primary );
	}
	

	private $_grid_columns = null;
	
	public function get_filter() {
		return $this->filter;
	}

	public function get_form() {
		return $this->_form;
	}
	
		function get_views() {
		$views = array();

		$form_id = $this->get_form_id();

		$filter_links = $this->get_filter_links();

		$filter = $this->filter;

		foreach ( $filter_links as $filter_link_index => $filter_link ) {
			$filter_arg = '&filter=';
			if ( $filter_link['id'] !== 'all' ) {
				$filter_arg .= $filter_link['id'];
			}
			if ( $filter == '' ) {
				$selected = $filter_link['id'] == 'all' ? 'current' : '';
			} else {
				$selected = ( $filter == $filter_link['id'] ) ? 'current' : '';
			}
			$link = '<a class="' . $selected . '" href="?page=gf_entries&view=entries&id=' . $form_id . esc_attr( $filter_arg ) . '">' . esc_html( $filter_link['label'] ) .
			        '<span class="count"> (<span	id="' . esc_attr( $filter_link['id'] ) . '_count">' . absint( rgar( $filter_link, 'count' ) ) . '</span>)</span></a>';
			$views[ $filter_link['id'] ] = $link;
		}
		return $views;
	}

	public function get_filter_links( $include_counts = true ) {

		$form = $this->get_form();

		return GFEntryList::get_filter_links( $form, $include_counts );
	}
	
	public function get_order() {
		return empty( $_GET['order'] ) ? 'ASC' : strtoupper( $_GET['order'] );
	}

	public function get_orderby() {
		return empty( $_GET['orderby'] ) ? 0 : $_GET['orderby'];
	}

	
	function prepare_items() {

		$this->process_action();

		$form_id = $this->get_form_id();

		$page_index = empty( $_GET['paged'] ) ? 0 : absint( $_GET['paged'] - 1 );

		$search_criteria = $this->get_search_criteria();

		$screen_options = get_user_option( 'gform_entries_screen_options' );
		$page_size      = isset( $screen_options['per_page'] ) ? absint( $screen_options['per_page'] ) : 30;

		$page_size        = gf_apply_filters( array( 'gform_entry_page_size', $form_id ), $page_size, $form_id );
		$first_item_index = $page_index * $page_size;

		$sort_field = $this->get_orderby();
		if ( ! empty( $sort_field ) ) {
			$sort_direction  = $this->get_order();
			$sort_field_meta = GFAPI::get_field( $form_id, $sort_field );

			if ( $sort_field_meta instanceof GF_Field ) {
				$is_numeric = $sort_field_meta->get_input_type() == 'number';
			} else {
				$entry_meta = GFFormsModel::get_entry_meta( $form_id );
				$is_numeric = rgars( $entry_meta, $sort_field . '/is_numeric' );
			}

			$sorting = array( 'key' => $sort_field, 'direction' => $sort_direction, 'is_numeric' => $is_numeric );
		} else {
			$sorting = array();
		}

		$paging      = array( 'offset' => $first_item_index, 'page_size' => $page_size );
		$total_count = 0;

		$args = gf_apply_filters( array( 'gform_get_entries_args_entry_list', $form_id ), compact( 'form_id', 'search_criteria', 'sorting', 'paging' ) );

		$entries = GFAPI::get_entries( $args['form_id'], $args['search_criteria'], $args['sorting'], $args['paging'], $total_count );

		$this->set_pagination_args( array(
			'total_items' => $total_count,
			'per_page'    => $args['paging']['page_size'],
		) );

		$this->items = $entries;
	}

		/**
	 * Returns the array of search criteria.
	 *
	 * @return array
	 */
	function get_search_criteria() {

		$search_criteria = array();

		$filter_links = $this->get_filter_links( false );

		foreach ( $filter_links as $filter_link ) {
			if ( $this->filter == $filter_link['id'] ) {
				$search_criteria['field_filters'] = $filter_link['field_filters'];
				break;
			}
		}

		$search_field_id = rgget( 'field_id' );

		$search_operator = rgget( 'operator' );

		$status = in_array( $this->filter, array( 'trash', 'spam' ) ) ? $this->filter : 'active';
		$search_criteria['status'] = $status;

		if ( isset( $_GET['field_id'] ) && $_GET['field_id'] !== '' ) {
			$key            = $search_field_id;
			$val            = stripslashes( rgget( 's' ) );
			$strpos_row_key = strpos( $search_field_id, '|' );
			if ( $strpos_row_key !== false ) { //multi-row likert
				$key_array = explode( '|', $search_field_id );
				$key       = $key_array[0];
				$val       = $key_array[1] . ':' . $val;
			}
			if ( 'entry_id' == $key ) {
				$key = 'id';
			}
			$filter_operator = empty( $search_operator ) ? 'is' : $search_operator;
			$form = $this->get_form();
			$field = GFFormsModel::get_field( $form, $key );
			if ( $field ) {
				$input_type = GFFormsModel::get_input_type( $field );
				if ( $field->type == 'product' && in_array( $input_type, array( 'radio', 'select' ) ) ) {
					$filter_operator = 'contains';
				}
			}

			$search_criteria['field_filters'][] = array(
				'key'      => $key,
				'operator' => $filter_operator,
				'value'    => $val,
			);

		}

		$form_id = $this->get_form_id();

		/**
		 * Allow the entry list search criteria to be overridden.
		 *
		 * @since  1.9.14.30
		 *
		 * @param array $search_criteria An array containing the search criteria.
		 * @param int $form_id The ID of the current form.
		 */
		$search_criteria = gf_apply_filters( array( 'gform_search_criteria_entry_list', $form_id ), $search_criteria, $form_id );

		return $search_criteria;
	}

	

	function get_columns() {
		$table_columns = array(
			'cb' => '<input type="checkbox" />',
		);

		$form_id = $this->get_form_id();
		$columns = $this->get_grid_columns();
		foreach ( $columns as $key => $column_info ) {
			$table_columns[ 'field_id-' . $key ] = $column_info['label'];
		}

		if ( empty( $columns ) ) {
			$table_columns['field_id-id'] = esc_html__( 'Entry Id', 'gravityforms' );
		}

		$column_selector_url = add_query_arg( array(
			'gf_page'   => 'select_columns',
			'id'        => absint( $form_id ),
			'TB_iframe' => 'true',
			'height'    => 465,
			'width'     => 620,
		), admin_url() );

		$table_columns['column_selector'] = '<a title="<div class=\'tb-title\'><div class=\'tb-title__logo\'></div><div class=\'tb-title__text\'><div class=\'tb-title__main\'>' . esc_attr__( 'Select Entry Table Columns', 'gravityforms' ) . '</div><div class=\'tb-title__sub\'>' . esc_attr( 'Drag & drop to order and select which columns are displayed in the entries table.', 'gravityforms' ) . '</div></div></div>" aria-label="' . esc_attr__( 'click to select columns to display', 'gravityforms' ) . '" href="' . esc_url( $column_selector_url ) . '" class="thickbox entries_edit_icon"><i class="gform-icon gform-icon--cog gform-icon--entries-edit"></i></a>';

		$table_columns = apply_filters( 'gform_entry_list_columns', $table_columns, $form_id );

		return apply_filters( 'gform_entry_list_columns_' . $form_id, $table_columns, $form_id );
	}

	function get_sortable_columns() {
		$columns = $this->get_grid_columns();
		$table_columns = array();
		foreach ( $columns as $key => $column_info ) {
			$table_columns[ 'field_id-' . (string) $key ] = array( (string) $key, false );
		}
		return $table_columns;
	}

	function _column_is_starred( $entry, $classes, $data, $primary ) {
		echo '<th scope="row" class="manage-column column-is_starred">';
		if ( $this->filter !== 'trash' ) {
			?>
			<img id="star_image_<?php echo esc_attr( $entry['id'] ) ?>" src="<?php echo GFCommon::get_base_url() ?>/images/star<?php echo intval( $entry['is_starred'] ) ?>.svg" onclick="ToggleStar(this, '<?php echo esc_js( $entry['id'] ); ?>','<?php echo esc_js( $this->filter ); ?>');" />
			<?php
		}
		echo '</th>';
	}

	function column_default( $entry, $column_id ) {
		$field_id = (string) str_replace( 'field_id-', '', $column_id );
		$form     = $this->get_form();
		$form_id  = $this->get_form_id();
		$field    = GFFormsModel::get_field( $form, $field_id );
		$columns  = $this->get_grid_columns();
		$value    = rgar( $entry, $field_id );

		if ( ! empty( $field ) && $field->type == 'post_category' ) {
			$value = GFCommon::prepare_post_category_value( $value, $field, 'entry_list' );
		}

		// Filtering lead value
		$value = apply_filters( 'gform_get_field_value', $value, $entry, $field );

		switch ( $field_id ) {

			case 'source_url' :
				$value = "<a href='" . esc_attr( $entry['source_url'] ) . "' target='_blank' alt='" . esc_attr( $entry['source_url'] ) . "'>.../" . esc_attr( GFCommon::truncate_url( $entry['source_url'] ) ) . '</a>';
				break;

			case 'date_created' :
			case 'payment_date' :
				$value = GFCommon::format_date( $value, false );
				break;

			case 'payment_amount' :
				$value = GFCommon::to_money( $value, $entry['currency'] );
				break;

			case 'payment_status' :
				$value = GFCommon::get_entry_payment_status_text( $entry['payment_status'] );
				break;

			case 'created_by' :
				if ( ! empty( $value ) ) {
					$userdata = get_userdata( $value );
					if ( ! empty( $userdata ) ) {
						$value = $userdata->user_login;
					}
				}
				break;

			default:
				if ( $field !== null ) {
					$value = $field->get_value_entry_list( $value, $entry, $field_id, $columns, $form );
				} else {
					$value = esc_html( $value );
				}
		}

		$value = apply_filters( 'gform_entries_field_value', $value, $form_id, $field_id, $entry );

		$primary      = $this->get_primary_column_name();
		$query_string = $this->get_detail_query_string( $entry );

		if ( $column_id == $primary ) {
			$edit_url = $this->get_detail_url( $entry );
			echo '<a aria-label="' . esc_attr__( 'View this entry', 'gravityforms' ) . '" href="' . $edit_url .'">' . $value . '</a>';
		} else {

			echo apply_filters( 'gform_entries_column_filter', $value, $form_id, $field_id, $entry, $query_string );

			echo '&nbsp; ';

			do_action( 'gform_entries_column', $form_id, $field_id, $value, $entry, $query_string );
		}

	}

	function get_detail_query_string( $entry ) {
		$form_id = $this->get_form_id();

		$search = stripslashes( rgget( 's' ) );

		$search_field_id = rgget( 'field_id' );
		$search_operator = rgget( 'operator' );

		$order   = $this->get_order();
		$orderby = $this->get_orderby();

		$search_qs  = empty( $search ) ? '' : '&s=' . esc_attr( urlencode( $search ) );
		$orderby_qs = empty( $orderby ) ? '' : '&orderby=' . esc_attr( $orderby );
		$order_qs   = empty( $order ) ? '' : '&order=' . esc_attr( $order );
		$filter_qs  = '&filter=' . esc_attr( $this->filter );

		$page_size  = $this->get_pagination_arg( 'per_page' );
		$page_num   = $this->get_pagenum();
		$page_index = $page_num - 1;

		$position = ( $page_size * $page_index ) + $this->row_index;

		$edit_url = 'page=gf_entries&view=entry&id=' . absint( $form_id ) . '&lid=' . esc_attr( $entry['id'] ) . $search_qs . $orderby_qs . $order_qs . $filter_qs . '&paged=' . $page_num .'&pos=' . $position .'&field_id=' . esc_attr( $search_field_id ) .  '&operator=' .  esc_attr( $search_operator );
		return $edit_url;
	}

	function get_detail_url( $entry ) {
		$query_string = $this->get_detail_query_string( $entry );
		$url          = admin_url( 'admin.php?' . $query_string );

		return $url;
	}

	public function single_row( $entry ) {
		$class = 'entry_row';
		$class .= $entry['is_read'] ? '' : ' entry_unread';
		$class .= $this->locking_info->list_row_class( $entry['id'], false );
		$class .= $entry['is_starred'] ? ' entry_starred' : '';
		$class .= in_array( $this->filter, array( 'trash', 'spam' ) ) ? ' entry_spam_trash' : '';
		echo sprintf( '<tr id="entry_row_%d" class="%s" data-id="%d">', $entry['id'], $class, $entry['id'] );
		$this->single_row_columns( $entry );
		echo '</tr>';
	}

	function column_cb( $entry ) {
		$entry_id = $entry['id'];
		?>
		<label class="screen-reader-text" for="cb-select-<?php echo esc_attr( $entry_id ); ?>"><?php _e( 'Select entry' ); ?></label>
		<input type="checkbox" class="gform_list_checkbox" name="entry[]" value="<?php echo esc_attr( $entry_id ); ?>" />
		<?php
		$this->locking_info->lock_indicator();
	}
	
	function column_column_selector( $entry ) {
		return '';
	}
	
	
	function get_bulk_actions() {
 
		$actions = array();

				$actions['add_to_slide'] = esc_html__( 'Add to slide', 'gravityforms' );

		// Get the current form ID.
		$form_id = $this->get_form_id();

		return gf_apply_filters( array( 'gform_entry_list_bulk_actions', $form_id ), $actions, $form_id );

	}
	function process_action() {

		$bulk_action = $this->current_action();
		$single_action = rgpost( 'single_action' );
		$select_all  = rgpost( 'all_entries' );
		$search_criteria = $this->get_search_criteria();
		$form_id = $this->get_form_id();

		$message_class = 'success';

		switch ( $bulk_action ) {
			case 'add_to_slide':
				$this->importSlides();

			break;
		}

	}

	function get_grid_columns() {
		if ( ! isset( $this->_grid_columns ) ) {
			$this->_grid_columns = GFFormsModel::get_grid_columns( $this->get_form_id(), true );
			
		}
		return $this->_grid_columns;
	}

	/**
 	* Returns an authorized API client.
 	* @return Google_Client the authorized client object
 	*/

	function getClient()
	{
		$client = new Google_Client();
		$client->setApplicationName('Slides for Wordpress');
		$client->addScope(Google\Service\Drive::DRIVE);
		$client->addScope(Google_Service_Slides::PRESENTATIONS);
		$client->addScope( 'https://www.googleapis.com/auth/spreadsheets' );
		$client->setAuthConfig(plugin_dir_path( dirname( __FILE__ ) ) . 'client_secret_769143310248-3fbmjvsoa5hoh6c40onq6ae0btfopr4a.apps.googleusercontent.com.json');
	
		$client->setAccessType('offline');
		$client->setPrompt('select_account consent');
	
		// Load previously authorized token from a file, if it exists.
		// The file token.json stores the user's access and refresh tokens, and is
		// created automatically when the authorization flow completes for the first
		// time.
		$tokenPath =  plugin_dir_path( dirname( __FILE__ ) ) .  'tryittoken.json';
		if (file_exists($tokenPath)) {
			$accessToken = json_decode(file_get_contents($tokenPath), true);
			$client->setAccessToken($accessToken);
		}
	
		// If there is no previous token or it's expired.
		if ($client->isAccessTokenExpired()) {
			// Refresh the token if possible, else fetch a new one.
			if ($client->getRefreshToken()) {
				$client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
			} else {
				$authUrl = $client->createAuthUrl();
				printf("Open the following link in your browser:\n%s\n", $authUrl);
				print 'Enter verification code: ';
				$authCode = trim(fgets(STDIN));
	
				// Exchange authorization code for an access token.
				$accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
				$client->setAccessToken($accessToken);
	
				// Check to see if there was an error.
				if (array_key_exists('error', $accessToken)) {
					throw new Exception(join(', ', $accessToken));
				}
			}
			// Save the token to a file.
			if (!file_exists(dirname($tokenPath))) {
				mkdir(dirname($tokenPath), 0700, true);
			}
			file_put_contents($tokenPath, json_encode($client->getAccessToken()));
		}
		return $client;
	}
	function clonePresentationByName(Google_Service_Drive $driveService, $copyName)
	{
		$response = $driveService->files->listFiles([
			'q' => "mimeType='application/vnd.google-apps.presentation' and name='!!!DONT TOUCH!!!! Story Acceleration Workshop Presentation Template (for site)'",
			'spaces' => 'drive',
			'fields' => 'files(id, name)',
		]);
		if($response->files){
			$templatePresentationId = $response->files[0]->id;
		} else {
			throw new Exception('Template presentation not found');
		}
	
		$copy = new Google_Service_Drive_DriveFile([
			'name' => $copyName
		]);
		$driveResponse = $driveService->files->copy($templatePresentationId, $copy);
	
		return $driveResponse->id;
	}
	
	function importSlides()
	{
		$entries = empty( $select_all ) ? $_POST['entry'] : GFAPI::get_entry_ids( $form_id, $search_criteria );
		$entry_count = count( $entries );
		$client = $this->getClient();
		$slidesService = new Google_Service_Slides($client);
		$driveService = new Google_Service_Drive($client);
		$sheetService = new Google_Service_Sheets($client);
		$pname = 'new presentation '. date('F j, Y');
		$newPresentation = $this->clonePresentationByName($driveService, $pname);
		$spreadsheetId = '1KAs7QHImwy0XJQLO7-PbMUKnvB6Gx6eVrFnBHMZTMxU';
		$requests = array();

		$requests[] = new Google_Service_Slides_Request(array(
			'insertTableRows' => array(
				'tableObjectId' => 'gf24ca6a1cc_0_40',
				'cellLocation' => array(
					'rowIndex' => 1,
				),
				'insertBelow' => false,
				'number' => $entry_count,

			)
		));
		$requests[] = new Google_Service_Slides_Request(array(
			'insertTableRows' => array(
				'tableObjectId' => 'g1043c98cf7d_2_0',
				'cellLocation' => array(
					'rowIndex' => 1,
				),
				'insertBelow' => false,
				'number' => $entry_count-1,

			),
		));
		$requests[] = new Google_Service_Slides_Request(array(
			'insertTableRows' => array(
				'tableObjectId' => 'g1043c98cf7d_9_0',
				'cellLocation' => array(
					'rowIndex' => 1,
				),
				'insertBelow' => false,
				'number' => $entry_count-1,

			)
		));
		$requests[] = new Google_Service_Slides_Request(array(
			'insertTableRows' => array(
				'tableObjectId' => 'g1043c98cf7d_9_1',
				'cellLocation' => array(
					'rowIndex' => 1,
				),
				'insertBelow' => false,
				'number' => $entry_count-1,

			)
		));
		$sum  = [];
        foreach ($entries as $row => $entry) {
		
			$results = $this->calculation($entry);
		
			$requests[] = new Google_Service_Slides_Request(array(
				'insertText' => array(
					'objectId' => 'g1043c98cf7d_2_0',
					'cellLocation' => array(
						'rowIndex' => $row,
						'columnIndex' => 0
					),
					'text' => $results['FRUSTRATING'],
					'insertionIndex' => 0
				),
				
			));
			$requests[] = new Google_Service_Slides_Request(array(
				'updateTextStyle' => array(
					'objectId' => 'g1043c98cf7d_2_0',
					'cellLocation' => array(
						'rowIndex' => $row,
						'columnIndex' => 0
					),
					'fields' => 'fontFamily,fontSize,bold',
					'textRange' => array(
						'type' => 'FIXED_RANGE',
						'startIndex' => '0',
						'endIndex' => $results['LNGNAME']
					),
					'style' => array(
						'fontFamily' => 'docs-Montserrat',
						'bold' => 'true',
						'fontSize' => array(
							'magnitude' => '12',
							'unit' => 'PT'
						),
					),
				),
			));
			$requests[] = new Google_Service_Slides_Request(array(
				'insertText' => array(
					'objectId' => 'g1043c98cf7d_9_1',
					'cellLocation' => array(
						'rowIndex' => $row,
						'columnIndex' => 0
					),
					'text' => $results['WHY'],
					'insertionIndex' => 0
				),
			));
			$requests[] = new Google_Service_Slides_Request(array(
				'updateTextStyle' => array(
					'objectId' => 'g1043c98cf7d_9_1',
					'cellLocation' => array(
						'rowIndex' => $row,
						'columnIndex' => 0
					),
					'fields' => 'fontFamily,fontSize,bold',
					'textRange' => array(
						'type' => 'FIXED_RANGE',
						'startIndex' => '0',
						'endIndex' => $results['LNGNAME']
					),
					'style' => array(
						'fontFamily' => 'docs-Montserrat',
						'bold' => 'true',
						'fontSize' => array(
							'magnitude' => '12',
							'unit' => 'PT'
						),
					),
				),
			));
			$requests[] = new Google_Service_Slides_Request(array(
				'insertText' => array(
					'objectId' => 'g1043c98cf7d_9_0',
					'cellLocation' => array(
						'rowIndex' => $row,
						'columnIndex' => 0
					),
					'text' => $results['CHALLENGE'],
					'insertionIndex' => 0
				),
			));
			$requests[] = new Google_Service_Slides_Request(array(
				'updateTextStyle' => array(
					'objectId' => 'g1043c98cf7d_9_0',
					'cellLocation' => array(
						'rowIndex' => $row,
						'columnIndex' => 0
					),
					'fields' => 'fontFamily,fontSize,bold',
					'textRange' => array(
						'type' => 'FIXED_RANGE',
						'startIndex' => '0',
						'endIndex' => $results['LNGNAME']
					),
					'style' => array(
						'fontFamily' => 'docs-Montserrat',
						'bold' => 'true',
						'fontSize' => array(
							'magnitude' => '12',
							'unit' => 'PT'
						),
					),
				),
			));
			foreach ($results['table'] as $column => $result) {
					
				$requests[] = new Google_Service_Slides_Request(array(
					'insertText' => array(
						'objectId' => 'gf24ca6a1cc_0_40',
						'cellLocation' => array(
							'rowIndex' => $row+1,
							'columnIndex' => $column
						),
						'text' => strval($result),
						'insertionIndex' => 0
					),
				));
				if (is_numeric($result)) {
					$sum[$column] += $result;
				}
			}
		
		}
        foreach ($sum as $column => $av_result) {
			$avarage = strval(round($av_result / $entry_count));
			$requests[] = new Google_Service_Slides_Request(array(
				'insertText' => array(
					'objectId' => 'gf24ca6a1cc_0_40',
					'cellLocation' => array(
						'rowIndex' => $entry_count+1,
						'columnIndex' => $column
					),
					'text' => $avarage,
					'insertionIndex' => 0
				),
			));
			$values[] = $avarage;

        }
		$requests[] = new Google_Service_Slides_Request(array(
			'replaceAllText' => array(
				'containsText' => array(
					'text' => '{{mh}}',
					'matchCase' => true
				),
				'replaceText' => $values[0]
			)
		));
		$requests[] = new Google_Service_Slides_Request(array(
			'replaceAllText' => array(
				'containsText' => array(
					'text' => '{{mv}}',
					'matchCase' => true
				),
				'replaceText' => $values[1]
			)
		));
		$requests[] = new Google_Service_Slides_Request(array(
			'replaceAllText' => array(
				'containsText' => array(
					'text' => '{{oh}}',
					'matchCase' => true
				),
				'replaceText' => $values[2]
			)
		));
		$requests[] = new Google_Service_Slides_Request(array(
			'replaceAllText' => array(
				'containsText' => array(
					'text' => '{{ov}}',
					'matchCase' => true
				),
				'replaceText' => $values[3]
			)
		));
		$requests[] = new Google_Service_Slides_Request(array(
			'replaceAllText' => array(
				'containsText' => array(
					'text' => '{{ch}}',
					'matchCase' => true
				),
				'replaceText' => $values[4]
			)
		));
		$requests[] = new Google_Service_Slides_Request(array(
			'replaceAllText' => array(
				'containsText' => array(
					'text' => '{{cv}}',
					'matchCase' => true
				),
				'replaceText' => $values[5]
			)
		));
		$requests[] = new Google_Service_Slides_Request(array(
			'replaceAllShapesWithImage' => array(
				'imageUrl' => 'https://votaryfilms.com/wp-content/uploads/slide-img/'.round($values[0]/10).'.png',
				'replaceMethod' => 'CENTER_INSIDE',
				'containsText' => array(
					'text' => '{{slide1}}',
					'matchCase' => true
				)

			)
		));
		$requests[] = new Google_Service_Slides_Request(array(
			'replaceAllShapesWithImage' => array(
				'imageUrl' => 'https://votaryfilms.com/wp-content/uploads/slide-img/'.round($values[1]/10).'-SM.png',
				'replaceMethod' => 'CENTER_INSIDE',
				'containsText' => array(
					'text' => '{{slide2}}',
					'matchCase' => true
				)

			)
		));
		$requests[] = new Google_Service_Slides_Request(array(
			'replaceAllShapesWithImage' => array(
				'imageUrl' => 'https://votaryfilms.com/wp-content/uploads/slide-img/'.round($values[2]/10).'.png',
				'replaceMethod' => 'CENTER_INSIDE',
				'containsText' => array(
					'text' => '{{slide3}}',
					'matchCase' => true
				)

			)
		));
		$requests[] = new Google_Service_Slides_Request(array(
			'replaceAllShapesWithImage' => array(
				'imageUrl' => 'https://votaryfilms.com/wp-content/uploads/slide-img/'.round($values[3]/10).'-SM.png',
				'replaceMethod' => 'CENTER_INSIDE',
				'containsText' => array(
					'text' => '{{slide4}}',
					'matchCase' => true
				)

			)
		));
		$requests[] = new Google_Service_Slides_Request(array(
			'replaceAllShapesWithImage' => array(
				'imageUrl' => 'https://votaryfilms.com/wp-content/uploads/slide-img/'.round($values[4]/10).'.png',
				'replaceMethod' => 'CENTER_INSIDE',
				'containsText' => array(
					'text' => '{{slide5}}',
					'matchCase' => true
				)

			)
		));
		$requests[] = new Google_Service_Slides_Request(array(
			'replaceAllShapesWithImage' => array(
				'imageUrl' => 'https://votaryfilms.com/wp-content/uploads/slide-img/'.round($values[5]/10).'-SM.png',
				'replaceMethod' => 'CENTER_INSIDE',
				'containsText' => array(
					'text' => '{{slide6}}',
					'matchCase' => true
				)

			)
		));
		// Execute the requests for this presentation.
		$batchUpdateRequest = new Google_Service_Slides_BatchUpdatePresentationRequest(array(
			'requests' => $requests
		));
		$response =
			$slidesService->presentations->batchUpdate($newPresentation, $batchUpdateRequest);

		
	}
	function calculation($entry){
		$entry_id = $entry;
		$result = GFAPI::get_entry($entry_id);
		$form = GFAPI::get_form($result['form_id']);

		$scopes_types = array(
			'culture',
			'culture_vis',
   	   	    'operations',
   			'operations_vis',
            'marketing',
            'marketing_vis',
    	);
   	    $scopes = [];
  	    $scopes_result = [];
    	$max_values = [];

    	foreach ($form['fields'] as $field) {
        	foreach ($scopes_types as $type) {
            	if (key_exists($type, $field) and $field[$type] == '1') {
                	$scopes[$field['id']][] = $type;
                	$max_values[$type] += 5;
            	}
        	}
    	}
		foreach ($result as $answer => $value) {
			if (key_exists($answer, $scopes)) {
				foreach ($scopes[$answer] as $scope) {
					$scopes_result[$scope][] = $value;
           		}
        	}
    	}	
		$test_results = ['table' => []];

		$test_results['LNGNAME'] = strlen($result['32']);

		$test_results['FRUSTRATING'] = $result['32']. ': ' .$result['53'];
		$test_results['CHALLENGE'] = $result['32']. ': ' .$result['54'];
		$test_results['WHY']= $result['32']. ': ' .$result['55'];

		$test_results['table'][] = $result['32'];

		$marketing = array_sum($scopes_result['marketing']);
		$test_results['table'][] = round($marketing / $max_values['marketing'], 2) * 100;

		$marketing_vis = array_sum($scopes_result['marketing_vis']);
		$test_results['table'][] = round($marketing_vis / $max_values['marketing_vis'], 2) * 100;

		$operations = array_sum($scopes_result['operations']);
		$test_results['table'][] = round($operations / $max_values['operations'], 2) * 100;

		$operations_vis = array_sum($scopes_result['operations_vis']);
		$test_results['table'][] = round($operations_vis / $max_values['operations_vis'], 2) * 100;
		
		$culture = array_sum($scopes_result['culture']);
		$test_results['table'][] = round($culture / $max_values['culture'], 2) * 100;

		$culture_vis = array_sum($scopes_result['culture_vis']);
		$test_results['table'][] = round($culture_vis / $max_values['culture_vis'], 2) * 100;

		return $test_results;
	}

	
}
