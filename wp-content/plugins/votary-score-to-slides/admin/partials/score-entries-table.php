<?php

if( !class_exists( 'WP_List_Table' ) ) {
    require_once ABSPATH . '/wp-admin/includes/class-wp-list-table.php';
}

class Score_Entities_Table extends WP_List_Table {
    public static function define_columns() {
        $columns = array(
            'cb' => '<input type="checkbox" />',
            'title' => 'Title'
        );

        return $columns;
    }

    public function __construct()
    {
        parent::__construct( array(
            'singular' => 'post',
            'plural' => 'posts',
            'ajax' => false
        ) );
    }


}
