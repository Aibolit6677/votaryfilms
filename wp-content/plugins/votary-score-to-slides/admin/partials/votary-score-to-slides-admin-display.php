<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://votaryfilms.com/
 * @since      1.0.0
 *
 * @package    Votary_Score_To_Slides
 * @subpackage Votary_Score_To_Slides/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
