<?php
require __DIR__ . '/vendor/autoload.php';

if (php_sapi_name() != 'cli') {
    throw new Exception('This application must be run on the command line.');
}

/**
 * Returns an authorized API client.
 * @return Google_Client the authorized client object
 */
function getClient()
{
    $client = new Google_Client();
    $client->setApplicationName('Google Slides API PHP Quickstart');
    //$client->setScopes(Google_Service_Slides::PRESENTATIONS);
    $client->addScope(Google\Service\Drive::DRIVE);
    //$client->setAuthConfig('client_secret_337686910822-udeosbnrdp9qigoo0mbjtjj4npjihgs4.apps.googleusercontent.com.json');
    $client->setAuthConfig('quickstart-324914-b7b1b4977281.json');

    $client->setAccessType('offline');
    $client->setPrompt('select_account consent');

    // Load previously authorized token from a file, if it exists.
    // The file token.json stores the user's access and refresh tokens, and is
    // created automatically when the authorization flow completes for the first
    // time.
    $tokenPath = 'token.json';
    if (file_exists($tokenPath)) {
        $accessToken = json_decode(file_get_contents($tokenPath), true);
        $client->setAccessToken($accessToken);
    }

    // If there is no previous token or it's expired.
    if ($client->isAccessTokenExpired()) {
        // Refresh the token if possible, else fetch a new one.
        if ($client->getRefreshToken()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        } else {
            // Request authorization from the user.
            $redirect_uri = 'http://localhost';
            $client->setRedirectUri($redirect_uri);
            $authUrl = $client->createAuthUrl();
            printf("Open the following link in your browser:\n%s\n", $authUrl);
            print 'Enter verification code: ';
            $authCode = trim(fgets(STDIN));

            // Exchange authorization code for an access token.
            $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
            $client->setAccessToken($accessToken);

            // Check to see if there was an error.
            if (array_key_exists('error', $accessToken)) {
                throw new Exception(join(', ', $accessToken));
            }
        }
        // Save the token to a file.
        if (!file_exists(dirname($tokenPath))) {
            mkdir(dirname($tokenPath), 0700, true);
        }
        file_put_contents($tokenPath, json_encode($client->getAccessToken()));
    }
    return $client;
}


// Get the API client and construct the service object.
$client = getClient();
$slidesService = new Google_Service_Slides($client);

//Create folder

$driveService = new Google_Service_Drive($client);

$files = $driveService->about->get();

print_r($files);
//$drivePermission = new \Google\Service\Drive\Permission();
//$drivePermission->setType('user');
//$drivePermission->setRole('reader');
//$drivePermission->setEmailAddress('greenmoonbase@gmail.com');
//
//
//$file = new Google_Service_Drive_DriveFile();
//$file->setName('APIFolder');
//$file->setMimeType('application/vnd.google-apps.folder');
//
//$file = $driveService->files->create($file);


//$title = 'New presentation';
//
//$presentation = new Google_Service_Slides_Presentation(array(
//    'title' => $title
//));
//
//
//$presentation = $slidesService->presentations->create($presentation);
//
//$pageId = $presentation->presentationId;
//
//$requests = array();
//$requests[] = new Google_Service_Slides_Request(array(
//    'createSlide' => array(
//        'objectId' => $pageId,
//        'insertionIndex' => 1,
//        'slideLayoutReference' => array(
//            'predefinedLayout' => 'TITLE_AND_TWO_COLUMNS'
//        )
//    )
//));


//// Prints the number of slides and elements in a sample presentation:
//// https://docs.google.com/presentation/d/1EAYk18WDjIG-zp_0vLm3CsfQh_i8eXc67Jo2O9C6Vuc/edit
//$presentationId = '1EAYk18WDjIG-zp_0vLm3CsfQh_i8eXc67Jo2O9C6Vuc';
//$presentation = $slidesService->presentations->get($presentationId);
//$slides = $presentation->getSlides();
//
//printf("The presentation contains %s slides:\n", count($slides));
//foreach ($slides as $i => $slide) {
//    // Print columns A and E, which correspond to indices 0 and 4.
//    printf("- Slide #%s contains %s elements.\n", $i + 1,
//        count($slide->getPageElements()));
//}
